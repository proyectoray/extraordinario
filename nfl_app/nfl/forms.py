from django import forms
from .models import Ciudad, Equipo, Estadio, Duenio, Jugador, Partido, Boleto, Liga

class CiudadForm(forms.ModelForm):
    class Meta:
        model = Ciudad
        fields = ["nombre"]
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre de la ciudad"}),
        }

class LigaForm(forms.ModelForm):
    class Meta:
        model = Liga
        fields = ["nombre", "ciudad"]
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre de la liga"}),
            "ciudad": forms.Select(attrs={"class": "form-control"}),
        }

class EquipoForm(forms.ModelForm):
    class Meta:
        model = Equipo
        fields = ["nombre", "ciudad", "duenio", "estadio", "liga"]  # Agregar "duenio" al formulario
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre del equipo"}),
            "ciudad": forms.Select(attrs={"class": "form-control"}),
            "liga": forms.Select(attrs={"class": "form-control"}),
            "estadio": forms.Select(attrs={"class": "form-control"}),
            "duenio": forms.Select(attrs={"class": "form-control"}),  # Agregar widget para Duenio
        }

class EstadioForm(forms.ModelForm):
    class Meta:
        model = Estadio
        fields = ["nombre", "duenio", "ciudad", "capacidad"]  # Agregar "duenio" al formulario
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre del estadio"}),
            "capacidad": forms.NumberInput(attrs={"class": "form-control", "placeholder": "Nombre del estadio"}),
            "duenio": forms.Select(attrs={"class": "form-control"}),  # Agregar widget para Duenio
            "ciudad": forms.Select(attrs={"class": "form-control"}),
        }

class JugadorForm(forms.ModelForm):
    class Meta:
        model = Jugador
        fields = ["nombre", "apellido", "alias", "equipo"]  # Agregar "apellido" y "alias" al formulario
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre del jugador"}),
            "apellido": forms.TextInput(attrs={"class": "form-control", "placeholder": "Apellido del jugador"}),  # Nuevo campo
            "alias": forms.TextInput(attrs={"class": "form-control", "placeholder": "Alias del jugador"}),      # Nuevo campo
            "equipo": forms.Select(attrs={"class": "form-control"}),
        }


class DuenioForm(forms.ModelForm):
    class Meta:
        model = Duenio
        fields = ["nombre", "apellido"]
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre del dueño"}),
            "apellido": forms.TextInput(attrs={"class": "form-control", "placeholder": "Apellido del dueño"}),
        }

class PartidoForm(forms.ModelForm):
    class Meta:
        model = Partido
        fields = ["equipo_local", "equipo_visitante", "estadio", "fecha"]
        widgets = {
            "equipo_local": forms.Select(attrs={"class": "form-control"}),
            "equipo_visitante": forms.Select(attrs={"class": "form-control"}),
            "estadio": forms.Select(attrs={"class": "form-control"}),
            "fecha": forms.SelectDateWidget()
            # Agrega widgets adicionales según tus necesidades
        }

class BoletoForm(forms.ModelForm):
    class Meta:
        model = Boleto
        fields = ['partido', 'cantidad']  # Agregar otros campos según sea necesario
        widgets = {
            'partido': forms.Select(attrs={'class': 'form-control'}),
            'cantidad': forms.NumberInput(attrs={'class': 'form-control'}),
            # Agregar widgets para otros campos según sea necesario
        }

