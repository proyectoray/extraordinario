from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import timedelta

# Create your models here.
from django.db import models

class Duenio(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)  # Agregamos el campo apellido

    def __str__(self):
        return f"{self.nombre} {self.apellido}"

class Ciudad(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    

    def __str__(self):
        return self.nombre

class Liga(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    ciudad = models.ForeignKey(Ciudad, on_delete=models.CASCADE)
    

    def __str__(self):
        return self.nombre

class Estadio(models.Model):
    id = models.AutoField(primary_key=True)
    capacidad = models.IntegerField()
    nombre = models.CharField(max_length=100)
    duenio = models.ForeignKey(Duenio, on_delete=models.CASCADE)
    ciudad = models.ForeignKey(Ciudad, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre
    
class Equipo(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    liga = models.ForeignKey(Liga, on_delete=models.CASCADE)
    duenio = models.ForeignKey(Duenio, on_delete=models.CASCADE)
    estadio = models.ForeignKey(Estadio, on_delete=models.CASCADE)
    ciudad = models.ForeignKey(Ciudad, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.nombre


    
class Jugador(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)  # Nuevo campo
    alias = models.CharField(max_length=50)      # Nuevo campo
    equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.nombre} {self.apellido})"
    
class Partido(models.Model):
    id = models.AutoField(primary_key=True)
    equipo_local = models.ForeignKey(Equipo, on_delete=models.CASCADE, related_name='partidos_local')
    equipo_visitante = models.ForeignKey(Equipo, on_delete=models.CASCADE, related_name='partidos_visitante')
    estadio = models.ForeignKey(Estadio, on_delete=models.CASCADE)
    fecha = models.DateTimeField()
    # Otros campos que desees agregar

    def __str__(self):
        return f"{self.equipo_local.nombre} vs {self.equipo_visitante.nombre} ({self.estadio.nombre})"

class Boleto(models.Model):
    id = models.AutoField(primary_key=True)
    partido = models.ForeignKey(Partido, on_delete=models.CASCADE)
    cantidad = models.IntegerField(default=1)  # Nuevo campo para la cantidad de boletos
    # Otros campos relacionados con el boleto que desees agregar

    def __str__(self):
        return f"{self.cantidad} boleto(s) para {self.partido}"



 
 