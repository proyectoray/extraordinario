from django.urls import path
from . import views

app_name = 'nfl' 

urlpatterns = [
    path('duenio/create/', views.CreateDuenio.as_view(), name='create_duenio'),
    path('duenio/list/', views.ListDuenios.as_view(), name='list_duenio'),
    path('duenio/<int:pk>/', views.DetailDuenio.as_view(), name='detail_duenio'),
    path('duenio/<int:pk>/update/', views.UpdateDuenio.as_view(), name='update_duenio'),
    path('duenio/<int:pk>/delete/', views.DeleteDuenio.as_view(), name='delete_duenio'),

    # URLs para Ciudad
    path('ciudad/create/', views.CreateCiudad.as_view(), name='create_ciudad'),
    path('ciudad/list/', views.ListCiudades.as_view(), name='list_ciudades'),
    path('ciudad/<int:pk>/', views.DetailCiudad.as_view(), name='detail_ciudad'),
    path('ciudad/update/<int:pk>/', views.UpdateCiudad.as_view(), name='update_ciudad'),
    path('ciudad/<int:pk>/delete/', views.DeleteCiudad.as_view(), name='delete_ciudad'),

    # URLs para Equipo
    path('equipo/create/', views.CreateEquipo.as_view(), name='create_equipo'),
    path('equipo/list/', views.ListEquipos.as_view(), name='list_equipo'),
    path('equipo/<int:pk>/', views.DetailEquipo.as_view(), name='detail_equipo'),
    path('equipo/<int:pk>/update/', views.UpdateEquipo.as_view(), name='update_equipo'),
    path('equipo/<int:pk>/delete/', views.DeleteEquipo.as_view(), name='delete_equipo'),

    # URLs para Estadio
    path('estadio/create/', views.CreateEstadio.as_view(), name='create_estadio'),
    path('estadio/list/', views.ListEstadios.as_view(), name='list_estadio'),
    path('estadio/<int:pk>/', views.DetailEstadio.as_view(), name='detail_estadio'),
    path('estadio/<int:pk>/update/', views.UpdateEstadio.as_view(), name='update_estadio'),
    path('estadio/<int:pk>/delete/', views.DeleteEstadio.as_view(), name='delete_estadio'),

    # URLs para Jugador
    path('jugador/create/', views.CreateJugador.as_view(), name='create_jugador'),
    path('jugador/list/', views.ListJugadores.as_view(), name='list_jugador'),
    path('jugador/<int:pk>/', views.DetailJugador.as_view(), name='detail_jugador'),
    path('jugador/<int:pk>/update/', views.UpdateJugador.as_view(), name='update_jugador'),
    path('jugador/<int:pk>/delete/', views.DeleteJugador.as_view(), name='delete_jugador'),
# URLs para Partido
    path('partido/create/', views.CreatePartido.as_view(), name='create_partido'),
    path('partido/list/', views.ListPartidos.as_view(), name='list_partido'),
    path('partido/<int:pk>/', views.DetailPartido.as_view(), name='detail_partido'),
    path('partido/<int:pk>/update/', views.UpdatePartido.as_view(), name='update_partido'),
    path('partido/<int:pk>/delete/', views.DeletePartido.as_view(), name='delete_partido'),

    path('boleto/create/', views.CreateBoleto.as_view(), name='create_boleto'),
    path('boleto/list/', views.ListBoletos.as_view(), name='list_boletos'),
    path('boleto/<int:pk>/', views.DetailBoleto.as_view(), name='detail_boleto'),
    path('boleto/<int:pk>/update/', views.UpdateBoleto.as_view(), name='update_boleto'),
    path('boleto/<int:pk>/delete/', views.DeleteBoleto.as_view(), name='delete_boleto'),

    path('liga/create/', views.CreateLiga.as_view(), name='create_liga'),
    path('liga/list/', views.ListLigas.as_view(), name='list_liga'),
    path('liga/<int:pk>/', views.DetailLiga.as_view(), name='detail_liga'),
    path('liga/<int:pk>/update/', views.UpdateLiga.as_view(), name='update_liga'),
    path('liga/<int:pk>/delete/', views.DeleteLiga.as_view(), name='delete_liga'),

    # ... Otras URLs existentes ...
]